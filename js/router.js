var fs = require('fs')
var path = require('path')

function toNote() {
  setScreen('note.html')
  require('./note-screen').init()
}

function toNoteList() {
  setScreen('note-list.html')
  require('./note-list-screen').init()
}

function setScreen(htmlPath) {
  let location = path.join(__dirname, '../html', htmlPath)
  let contents = fs.readFileSync(location, 'utf-8')
  document.getElementById('app').innerHTML = contents
}

module.exports = {
  toNote: toNote,
  toNoteList: toNoteList
}