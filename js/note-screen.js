const sharedMemory = require('./shared-memory')
const storage = require('./storage')
const router = require('./router')

window.jQuery = window.$ = require('jquery')
require('materialize-css')

let titleInput = null;
let bodyInput = null;
let pinIcon = null;
let activeNote = null;

function init() {
  activeNote = sharedMemory.getActiveNote()
  titleInput = document.getElementById('title')
  bodyInput = document.getElementById('body')
  pinIcon = document.getElementById('pin-icon')

  titleInput.value = activeNote.title
  bodyInput.value = activeNote.body
  updatePinIcon()

  title.addEventListener('input', onNoteChanged)
  body.addEventListener('input', onNoteChanged)
  document.getElementById('back-btn').addEventListener('click', onBackPressed)
  document.getElementById('delete-btn').addEventListener('click', onDeletePressed)
  document.getElementById('pin-btn').addEventListener('click', onPinPressed)
  document.addEventListener('keyup', onDocumentKeyPressed)

  resizeBodyToFit()

  if (activeNote.isEmpty()) {
    title.focus()
  } else {
    body.focus()
    window.scrollTo(0, 0)
  }
}

function onNoteChanged() {
  storage.updateNote(activeNote, titleInput.value, bodyInput.value, (err, note) => {
    if (err) {
      return alert('There was a problem saving your note.')
    }
    activeNote = note
  })
  resizeBodyToFit()
}

function onBackPressed() {
  if (activeNote.isEmpty()) {
    onDeletePressed()
  } else {
    router.toNoteList()
  }
}

function onDeletePressed() {
  storage.deleteNote(activeNote, (err) => {
    if (err) {
      return alert('There was a problem deleting your note.')
    }
    router.toNoteList()
  })
}

function onPinPressed() {
  storage.updateNotePinned(activeNote, !activeNote.pinned, (err, note) => {
    if (err) {
      return alert('There was a problem changing the pinned status.')
    }
    activeNote = note
    updatePinIcon()
  })
}

function onDocumentKeyPressed(e) {
  // Exit note when ESC is pressed
  if (e.keyCode === 27) {
    onBackPressed()
  }
}

function resizeBodyToFit() {
  bodyInput.style.height = '24px'
  bodyInput.style.height = bodyInput.scrollHeight + 12 + 'px'
}

function updatePinIcon() {
  pinIcon.innerHTML = activeNote.pinned ? 'star' : 'star_border'
}

module.exports = {
  init: init
}