const fs = require('fs')
const path = require('path')
const async = require('async')
const Note = require('./model/note').Note
const settings = require('./settings')
const Datastore = require('nedb')
const db = {};

function rebuildFromDisk(dir, callback) {
  async.series({
    remove1: (callback) => {
      getDb().notes.remove({}, { multi: true }, callback)
    },
    remove2: (callback) => {
      getDb().notes.ensureIndex({ fieldName: 'path', unique: true }, callback)
    },
    remove3: (callback) => {
      getDb().tags.remove({}, { multi: true }, callback)
    },
    remove4: (callback) => {
      getDb().tags.ensureIndex({ fieldName: 'name', unique: true }, callback)
    },
    queryResults: (callback) => {
      async.waterfall([
          (callback) => {
            fs.readdir(dir, callback)
          },
          (dirListing, callback) => {
            // Filter hidden files
            dirListing = dirListing.filter((file) => {
              return !file.startsWith('.')
            })

            // Join the directory path with the file path
            callback(null, dirListing.map((file) => {
              return path.join(dir, file)
            }))
          },
          (files, callback) => {
            // Map file paths to file contents
            async.map(files, (file, callback) => {
              fs.readFile(file, 'utf-8', (err, content) => {
                callback(err, { filePath: file, content: content })
              })
            }, callback)
          },
          (contentWrappers, callback) => {
            // Map file contents to structured Note objects
            async.map(contentWrappers, (contentWrapper, callback) => {
              let note = new Note(contentWrapper.filePath)
              Note.fillFromString(note, contentWrapper.content)
              let stat = fs.statSync(contentWrapper.filePath)
              note.createdTimestampMs = stat.ctime.getTime()
              note.updatedTimestampMs = stat.mtime.getTime()
              getDb().notes.insert(note, callback)
            }, callback)
          },
          (notes, callback) => {
            callback(null, notes.map((note) => {
              return Note.clone(note)
            }))
          }], callback)
    }
  }, (err, results) => {
    if (err) {
      return callback(err, null)
    }
    callback(null, results.queryResults)
  })
}

function createNote(callback) {
  // Get available filename
  let name = Date.now() + ''
  let suffix = ''
  let noteDir = settings.getNoteDir()
  let copy = 1
  while(fs.existsSync(path.join(noteDir, name) + suffix + '.md')) {
    suffix = '_' + copy
    copy++
  }

  // Write an empty file to disk
  let filePath = path.join(noteDir, name) + suffix + '.md'
  fs.writeFile(filePath, ' ', (err) => {
    if (err) {
      return callback(err, null)
    }
    let note = new Note(filePath)
    getDb().notes.insert(note, (err, note) => {
      if (err) {
        return callback(err, null)
      }
      callback(null, Note.clone(note))
    })
  })
}

function getPinnedNotes(callback) {
  return getDb().notes.find({ pinned: true }).sort({ updatedTimestampMs: -1 }).exec((err, notes) => {
    if (err) {
      return callback(err, [])
    }
    callback(null, mapNoteObjects(notes))
  })
}

function getUnpinnedNotes(callback) {
  return getDb().notes.find({ pinned: false }).sort({ updatedTimestampMs: -1 }).exec((err, notes) => {
    if (err) {
      return callback(err, [])
    }
    callback(null, mapNoteObjects(notes))
  })
}

function getNoteByPath(filePath, callback) {
  getDb().notes.findOne({ path: filePath }, (err, note) => {
    if (err) {
      return callback(err, null)
    }
    callback(null, Note.clone(note))
  })
}

function updateNote(oldNote, title, body, callback) {
  let note = Note.clone(oldNote)
  note.title = title
  note.body = body
  fs.writeFileSync(note.path, note.toString())

  getDb().notes.update({ path: oldNote.path }, note, (err, numReplaced) => {
    if (err || numReplaced == 0) {
      return callback(err || "No notes were updated", null)
    }
    callback(null, note)
  })
}

function updateNotePinned(note, pinned, callback) {
  note.pinned = pinned;
  fs.writeFileSync(note.path, note.toString())

  getDb().notes.update({ path: note.path }, note, (err, numReplaced) => {
    if (err || numReplaced == 0) {
      return callback(err || "No notes were updated", null)
    }
    callback(null, note)
  })
}

function deleteNote(note, callback) {
  fs.unlinkSync(note.path)

  getDb().notes.remove({ path: note.path }, (err, numRemoved) => {
    if (err || numRemoved == 0) {
      return callback(err || "No notes were deleted")
    }
    callback(null)
  })
}

function mapNoteObjects(notes) {
  return notes.map((note) => {
    return Note.clone(note)
  })
}

function getDb(callback) {
  if (!db.notes) {
    db.notes = new Datastore({
      filename: path.join(getDbDir(), 'notes.db'),
      autoload: true
    })
  }
  if (!db.tags) {
    db.tags = new Datastore({
      filename: path.join(getDbDir(), 'tags.db'),
      autoload: true
    })
  }
  return db
}

function getDbDir() {
  return path.join(settings.getNoteDir(), '.desktop/', 'db/')
}

module.exports = {
  rebuildFromDisk: rebuildFromDisk,
  getPinnedNotes: getPinnedNotes,
  getNoteByPath: getNoteByPath,
  getUnpinnedNotes: getUnpinnedNotes,
  createNote: createNote,
  updateNote: updateNote,
  updateNotePinned: updateNotePinned,
  deleteNote: deleteNote
}