function getNoteDir() {
  return localStorage.getItem('noteDir')
}

function setNoteDir(noteDir) {
  localStorage.setItem('noteDir', noteDir)
}

module.exports = {
  getNoteDir: getNoteDir,
  setNoteDir: setNoteDir
}