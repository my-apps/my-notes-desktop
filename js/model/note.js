const HEADER_BOUNDARY = '----------'

class Note {

  constructor(path) {
    this.path = path
    this.title = ''
    this.tags = []
    this.pinned = false
    let now = Date.now()
    this.createdTimestampMs = now
    this.updatedTimestampMs = now
    this.body = ''
  }

  isEmpty() {
    return (!this.title || this.title.trim().length == 0) 
        && (!this.body || this.body.trim().length == 0)
  }

  toString() {
    let out = HEADER_BOUNDARY + '\n'
    out += JSON.stringify({
      title: this.title,
      tags: this.tags,
      pinned: this.pinned
    }, null, 2) + '\n'
    out += HEADER_BOUNDARY + '\n'
    out += this.body
    return out
  }

  static fillFromString(note, contents) {
    let lines = contents.split('\n')
    if (lines.length == 0) {
      return note
    }

    if (lines[0] == HEADER_BOUNDARY) {
      let header = ''
      let i = 1
      while (i < lines.length && lines[i] !== HEADER_BOUNDARY) {
        header += lines[i]
        i++
      }
      header = JSON.parse(header)
      note.title = header.title ? header.title : ''
      note.tags = header.tags ? header.tags : []
      note.pinned = header.pinned ? header.pinned : false
      note.body = lines.slice(i + 1).join('\n')
    } else {
      note.body = contents
    }
    return note
  }

  static clone(old) {
    let note = new Note(old.path)
    note.title = old.title
    note.tags = old.tags
    note.pinned = old.pinned
    note.body = old.body
    note.createdTimestampMs = old.createdTimestampMs
    note.updatedTimestampMs = old.updatedTimestampMs
    return note
  }
}

module.exports = {
  Note: Note
}