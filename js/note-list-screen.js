const {dialog} = require('electron').remote
const Handlebars = require('handlebars')
const salvattore = require('salvattore')
const storage = require('./storage')
const settings = require('./settings')
const router = require('./router')
const sharedMemory = require('./shared-memory')
const async = require('async')

window.jQuery = window.$ = require('jquery')
require('materialize-css')

function init() {
  getNoteDir((noteDir) => {
    storage.rebuildFromDisk(noteDir, (err, results) => {
      getNotes(noteDir)
    })
  })
  document.getElementById('create-note-btn').addEventListener('click', onCreateNoteClick)
  document.getElementById('folder-btn').addEventListener('click', onFolderClick)
}

function getNoteDir(callback) {
  let noteDir = settings.getNoteDir()
  if (noteDir) {
    return callback(noteDir);
  }
  selectFolder((err, noteDir) => {
    if (err) {
      alert('You need to select a folder.')
      return getNoteDir(callback)
    }
    callback(noteDir)
  })
}

function getNotes(dir) {
  storage.getPinnedNotes((err, notes) => {
    showNotes(notes, document.getElementById('note-list-pinned'))
  })
  storage.getUnpinnedNotes((err, notes) => {
    showNotes(notes, document.getElementById('note-list-unpinned'))
  })
}

function showNotes(notes, container) {
  container.innerHTML = ''
  let template = Handlebars.compile(document.getElementById('note-template').innerHTML)
  for (let note of notes) {
    container.innerHTML += template(note)
  }
  salvattore.recreateColumns(container)
}

function onCreateNoteClick(e) {
  storage.createNote((err, note) => {
    console.log(note)
    sharedMemory.setActiveNote(note)
    router.toNote()
  })
}

function onFolderClick(e) {
  selectFolder((err, noteDir) => {
    if (err) {
      return alert('No folder was selected.')
    }
    getNotes(noteDir)
  })
}

function onNoteEditClick(element) {
  storage.getNoteByPath(element.dataset.path, (err, note) => {
    if (err) {
      return console.error(err)
    }
    sharedMemory.setActiveNote(note)
    router.toNote()
  })
}

function selectFolder(callback) {
  let noteDir = settings.getNoteDir()
  dialog.showOpenDialog({
    defaultPath: noteDir || '~/',
    buttonLabel: 'Select Folder',
    properties: ['openDirectory']
  }, function(filePaths) {
    if (!filePaths || filePaths.length === 0) {
      callback('No folder selected')
    }
    noteDir = filePaths[0]
    settings.setNoteDir(noteDir)
    storage.rebuildFromDisk(noteDir, (err, results) => {
      callback(null, noteDir)
    })
  })
}

module.exports = {
  init: init
}
window.onNoteEditClick = onNoteEditClick
