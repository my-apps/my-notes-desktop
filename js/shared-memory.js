let activeNote = null;

function getActiveNote() {
  return activeNote;
}

function setActiveNote(note) {
  activeNote = note
}


module.exports = {
  getActiveNote: getActiveNote,
  setActiveNote: setActiveNote
}